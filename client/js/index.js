function _(el) {
    return document.querySelector(el);
}

function removeElement(el) {
    el.parentElement.removeChild(el);
}

function unloadMessages() {
    var messages = Array.from(document.getElementsByClassName("message"));

    if (messages.length > 100) {
        removeElement(messages[0]);
    } 

}

function addMessage(username, messageText) {
    var messages = document.getElementsByClassName("message");
    var lastMessage = messages[messages.length - 1];
    var lastMessageAuthor = lastMessage.getElementsByClassName("username")[0].innerHTML;
    var contentLength = Array.from(lastMessage.getElementsByClassName("text")).length;

    if (lastMessageAuthor === username && contentLength <= 5) {
        var newText = document.createElement("div");
        newText.classList.add("text");
        newText.innerHTML = messageText;

        var lastMessageContent = lastMessage.getElementsByClassName("content")[0];
        lastMessageContent.appendChild(newText);
    } else {
/*         var hr = document.createElement("hr");
        chatContainer.appendChild(hr); */

        var newMessage = document.createElement("div");
        newMessage.classList.add("message");

        var profilePic = document.createElement("div");
        profilePic.classList.add("profile-pic");
        var profilePicImg = document.createElement("img");
        profilePicImg.src = "https://cdn.discordapp.com/avatars/435771475555778560/ef502bf1dbcd012ab4b01b4bb2e110cd.webp?size=128";
        profilePic.appendChild(profilePicImg);
        newMessage.appendChild(profilePic);

        var contentDiv = document.createElement("div");
        contentDiv.classList.add("content");

        var upperDiv = document.createElement("div");
        upperDiv.classList.add("upper");

        var usernameDiv = document.createElement("span");
        usernameDiv.classList.add("username");
        usernameDiv.innerHTML = username;
        upperDiv.appendChild(usernameDiv);

        var dateSpan = document.createElement("span");
        dateSpan.classList.add("date");
        dateSpan.innerHTML = "Today at 1:55 AM";
        upperDiv.appendChild(dateSpan);

        var optionsSpan = document.createElement("span");
        optionsSpan.classList.add("options");

        var optionsFa = document.createElement("i");
        optionsFa.classList.add("fa", "fa-ellipsis-v");
        optionsFa.setAttribute("aria-hidden", true);
        optionsSpan.appendChild(optionsFa);
        upperDiv.appendChild(optionsSpan);
        contentDiv.appendChild(upperDiv);

        var textDiv = document.createElement("div");
        textDiv.classList.add("text");
        textDiv.innerHTML = messageText;
        contentDiv.appendChild(textDiv);
        newMessage.appendChild(contentDiv);
        chatContainer.appendChild(newMessage);
    }

    chatContainer.scrollTop = chatContainer.scrollHeight;
}

const chatContainer = _("#chat");
const inputMessage = _("#inputMessage");
const buttonMentions = _("#buttonMentions");
const rightContainer = _("#rightContainer");
const usersOnline = _("#usersOnline");

inputMessage.addEventListener("keydown", (event) => {
    if (event.key === "Enter") {
        addMessage("Senk Ju", inputMessage.value);
        unloadMessages();
        inputMessage.value = "";
    }
});

buttonMentions.addEventListener("click", (event) => {
    var button = event.target;

    var offsetX = (button.clientWidth / 2) + button.offsetLeft - 150;
    var offsetY = button.offsetTop + 40;

    var panel = document.createElement("div");
    panel.classList.add("panel-top", "panel");

    panel.style.left = `${offsetX}px`;
    panel.style.top = `${offsetY}px`;

    var panelTitle = document.createElement("div");
    panelTitle.classList.add("title");
    panelTitle.innerHTML = "Mentions";
    panel.appendChild(panelTitle);

    rightContainer.appendChild(panel);
});

usersOnline.addEventListener("click", (event) => {
    var button = event.target;

    var offsetX = (button.clientWidth / 2) + button.offsetLeft - 150;
    var offsetY = button.offsetTop + 40;

    var panel = document.createElement("div");
    panel.classList.add("panel-top", "panel");

    panel.style.left = `${offsetX}px`;
    panel.style.top = `${offsetY}px`;

    var panelTitle = document.createElement("div");
    panelTitle.classList.add("title");
    panelTitle.innerHTML = "Users Online";
    panel.appendChild(panelTitle);

    rightContainer.appendChild(panel);
});

document.body.addEventListener("click", (event) => {
    if (event.target.classList.contains("button")) return;

    var panels = Array.from(document.getElementsByClassName("panel"));

    panels.forEach(panel => {
        panel.style.opacity = "0";

        setTimeout(() => {
            removeElement(panel);
        }, 400);
    });
});
